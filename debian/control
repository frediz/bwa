Source: bwa
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Charles Plessy <plessy@debian.org>,
           Andreas Tille <tille@debian.org>,
           Carlos Borroto <carlos.borroto@gmail.com>,
           Ognyan Kulev <ogi@debian.org>,
           Michael R. Crusoe <crusoe@ucdavis.edu>
Section: science
Priority: optional
Build-Depends: debhelper (>= 11~),
               zlib1g-dev
Standards-Version: 4.2.1
Vcs-Browser: https://salsa.debian.org/med-team/bwa
Vcs-Git: https://salsa.debian.org/med-team/bwa.git
Homepage: http://bio-bwa.sourceforge.net/

Package: bwa
Architecture: amd64 kfreebsd-amd64
Depends: ${shlibs:Depends},
         ${misc:Depends}
Suggests: samtools
Description: Burrows-Wheeler Aligner
 BWA is a software package for mapping low-divergent sequences against
 a large reference genome, such as the human genome. It consists of
 three algorithms: BWA-backtrack, BWA-SW and BWA-MEM. The first
 algorithm is designed for Illumina sequence reads up to 100bp, while
 the rest two for longer sequences ranged from 70bp to 1Mbp. BWA-MEM
 and BWA-SW share similar features such as long-read support and split
 alignment, but BWA-MEM, which is the latest, is generally recommended
 for high-quality queries as it is faster and more accurate. BWA-MEM
 also has better performance than BWA-backtrack for 70-100bp Illumina
 reads.

Package: libbwa-dev
Architecture: amd64 kfreebsd-amd64
Section: libdevel
Depends: ${shlibs:Depends},
         ${misc:Depends}
Suggests: samtools
Description: Burrows-Wheeler Aligner source files
 BWA is a software package for mapping low-divergent sequences against
 a large reference genome, such as the human genome. It consists of
 three algorithms: BWA-backtrack, BWA-SW and BWA-MEM. The first
 algorithm is designed for Illumina sequence reads up to 100bp, while
 the rest two for longer sequences ranged from 70bp to 1Mbp. BWA-MEM
 and BWA-SW share similar features such as long-read support and split
 alignment, but BWA-MEM, which is the latest, is generally recommended
 for high-quality queries as it is faster and more accurate. BWA-MEM
 also has better performance than BWA-backtrack for 70-100bp Illumina
 reads.
 .
 This package contains the source files for use in other programs.
